import java.util.Date;
import java.io.Serializable;

public class Persona implements Serializable {  
    String nombre;
    Date fdn;
    
    public Persona(String a, Date b){
        this.nombre = a;
        this.fdn = b;
    }
    
    public Persona(String a){
         this.nombre = a;
    }
    
    public String getNombre(){ 
          return this.nombre;
    }
    
    public void setNombre(String a){
         this.nombre = a;
    }
    
    public Date getFdn(){ 
          return this.fdn;
    }
    
    public void setFdn(Date a){
         this.fdn = a;
    }
    
    public boolean equals(Persona a){
        return  a.nombre.equals( nombre ) && a.fdn.equals( fdn );
    }
    
    public String toString(){
        return nombre + " - "+ fdn;
    }
}