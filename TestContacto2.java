import java.util.Date;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

public class TestContacto2
{
    public static void main(String... args){
        String nombre;
        Date fdn;
        String email;
        long telefono;
           
        Contacto contacto = null;
        
        FileInputStream fis = null; 
        ObjectInputStream entrada = null; 
        
        try{
           fis = new FileInputStream("data.ser");
           entrada = new ObjectInputStream(fis);
           
           contacto =  (Contacto) entrada.readObject();
           System.out.println("CONTACTO: "+"\n"+contacto.toString() );
        }
        
        catch(Exception e ){
            e.printStackTrace();
        }
        
        finally{
            try{
               if (entrada != null)
                  entrada.close();
               if (fis != null)
                  fis.close();
            }
            catch(IOException e){
               e.printStackTrace();
            }
        }
        
        
        
    }
}
